<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PollingController@index')->name('agreement');
Route::get('/decline', 'PollingController@declinePolling')->name('decline');
Route::post('/decline', 'PollingController@postDeclineReason')->name('post.decline');
Route::get('/polling', 'PollingController@startPolling')->name('index');
Route::post('/', 'PollingController@postPolling')->name('polling.post');
Route::post('/postAnswer', 'PollingController@postAnswer')->name('polling.answer.post');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
