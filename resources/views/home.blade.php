@extends('layouts.app')

@section('content')
    <questions :questions="{{$questions}}"
               :questions-with-answers="{{$questionsWithAnswers}}"
               :questions-with-other="{{$questionsWithOther}}"
               :other-answer-ids="{{$otherAnswerIds}}"
               :special-answer-ids="{{$specialAnswerIds}}"
               :q16-ids="{{$q16}}"
               :new-polling="{{ $newPolling ? 'true' : 'false' }}"></questions>
    {{--{{dd($questions)}}--}}
@endsection
