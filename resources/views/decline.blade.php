@extends('layouts.app')

@section('content')

    <decline-question :questions="{{$questions}}"
               :questions-with-answers="{{$questionsWithAnswers}}"
              ></decline-question>
@endsection
