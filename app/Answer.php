<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{

    const OTHER_ANSWER_IDS = [314,
        332,
        347,
        407,
    ];
//    const OTHER_ANSWER_ID = 407;
//    const YES_ANSWER_ID = 280;
    const SPECIAL_ANSWER_IDS = [
        281,
        282,
        283,
        303,
        304,
        305,
        306,
    ];
    const I_AGREE_ANSWER_ID = 284;
    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }
}
