<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Polling;
use App\Question;
use Illuminate\Http\Request;

class PollingController extends Controller
{
    public function index(Request $request)
    {
        return view('welcome');
    }

    public function declinePolling(Request $request)
    {
        $questions = Question::with(['answers' => function($query) {
            $query->orderBy('answer_order', 'asc');
        }])->where('id', Question::DECLINER_REASON_QUESTION)->orderBy('id', 'asc')->get()->toTree()->toJson();

        $questionsWithAnswers = Question::select('questions.id', 'questions.answersCount')
            ->has('answers')
            ->where('id', Question::DECLINER_REASON_QUESTION)
            ->get()
            ->keyBy('id')
            ->map(function ($item, $key) {
                if ($item->answersCount > 1) {
                    return [];
                }
                return 0;
            })
            ->toJson();
        return view('decline', compact('questions',
            'questionsWithAnswers'));
    }

    public function startPolling(Request $request)
    {
        $questions = Question::with(['answers' => function($query) {
            $query->orderBy('answer_order', 'asc');
        }])
            ->where('id', '<>', Question::DECLINER_REASON_QUESTION)
            ->orderBy('id', 'asc')->get()->toTree()->toJson();
        $newPolling = true;
        if (session()->has('polling')) {
            $questionsWithAnswers = collect(session('polling'))->toJson();
            $newPolling = false;
        } else {
            $questionsWithAnswers = Question::select('questions.id', 'questions.answersCount')
                ->has('answers')
                ->where('id', '<>', Question::DECLINER_REASON_QUESTION)
                ->get()
                ->keyBy('id')
                ->map(function ($item, $key) {
                    if ($item->answersCount > 1) {
                        return [];
                    }
                    return 0;
                })
                ->toJson();
        }
        $otherAnswerIds = collect(Answer::OTHER_ANSWER_IDS)->toJson();
        $specialAnswerIds = collect(Answer::SPECIAL_ANSWER_IDS)->toJson();
        $q16 = collect(Question::QUESTION_16)->toJson();
        $questionsWithOther = collect(['o'=>'others'])->toJson();
        if (session()->has('others')) {
            $questionsWithOther = collect(session('others'))->toJson();
        }
        return view('home', compact('questions',
            'questionsWithAnswers',
            'otherAnswerIds',
            'questionsWithOther',
            'newPolling',
            'specialAnswerIds',
            'q16'
            )
        );
    }

    public function postAnswer(Request $request) {
        session()->put('polling', $request->polling);
        session()->put('others', $request->others);
    }

    public function postPolling(Request $request) {
    //    if (
    //        !in_array(Answer::SPECIAL_ANSWER_ID, $request->polling) &&
    //        in_array([], $request->polling) ||
    //        in_array(0, $request->polling)
    //    ) {
    //        return response()->json(['error' => 'Вы ответили не на все вопросы!'], 422);
    //    }

        $polling = Polling::create(['respondent' => microtime()]);
        foreach ($request->polling as $question => $answers){
            foreach ((array)$answers as $answer) {
                $pivot = ['answer_id' => $answer];
                if (in_array($answer, Answer::OTHER_ANSWER_IDS)) {
                    $pivot['other_text'] = $request->others[$question];
                }
                if ($answer === 0) {
                    continue;
                }
                $polling->questions()->attach($question, $pivot);
            };
        }

        session()->flush();
        return response()->json([], 200);
    }

    public function postDeclineReason(Request $request) {

        session()->flush();
//        dd(in_array(Answer::I_AGREE_ANSWER_ID, $request->polling), $request->polling);
        if (in_array(Answer::I_AGREE_ANSWER_ID, $request->polling)) {
            return response()->json(['redirectUrl' => route('index')], 200);
//            return redirect(route('index'));
        }
        $polling = Polling::create(['respondent' => microtime()]);
        foreach ($request->polling as $question => $answers){
            foreach ((array)$answers as $answer) {
                $pivot = ['answer_id' => $answer];
                $polling->questions()->attach($question, $pivot);
            };
        }
        return response()->json(['redirectUrl' => route('agreement')], 200);
    }
}
