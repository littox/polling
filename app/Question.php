<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Question extends Model
{
    use NodeTrait;

    const DECLINER_REASON_QUESTION = 209;
    const QUESTION_16 = [
        298,
        299,
        300
    ];
//    const QUESTION_16 = 298;
//    const QUESTION_16 = 298;
    public function answers()
    {
        return $this->belongsToMany(Answer::class)->withPivot('answer_order');
    }
    public function question()
    {
        return $this->belongsTo(Question::class, 'parent_id');
    }
    public function parentId()
    {
        return $this->belongsTo(Question::class, 'parent_id', 'id');
    }
}
