<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polling extends Model
{
    protected $guarded = ['id'];


    public function questions()
    {
        return $this->belongsToMany(Question::class, 'polling_results')->withPivot('answer_id');
    }
}
